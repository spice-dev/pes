# include all important stuff

include Makefile.inc

aputils:
	cd ./src/; \
	$(MPIF90) $(MPIF_INCLUDE) -c $(DEBUG_FLAGS) $(FC_FLAGS) ap_utils.f90

getoptions:
	cd ./src/; \
	$(MPIF90) $(MPIF_INCLUDE) -c $(DEBUG_FLAGS) $(FC_FLAGS) getoptions.f90

sorting: aputils
	cd ./src/; \
	$(MPIF90) $(MPIF_INCLUDE) -c $(RELEASE_FLAGS) $(FC_FLAGS) sorting.f90

sorting-debug: aputils
	cd ./src/; \
	$(MPIF90) $(MPIF_INCLUDE) -c $(DEBUG_FLAGS) $(FC_FLAGS) sorting.f90

smtools: sorting
	cd ./src/; \
	$(MPIF90) $(MPIF_INCLUDE) $(MATIO_LIBS) -c $(RELEASE_FLAGS) $(FC_FLAGS) sm_tools.f90

smtools-debug: sorting-debug
	cd ./src/; \
	$(MPIF90) $(MPIF_INCLUDE) $(MATIO_LIBS) -c $(DEBUG_FLAGS) $(FC_FLAGS) sm_tools.f90

pesm: aputils smtools-debug
	cd ./src/; \
	$(MPIF90) $(MPIF_INCLUDE) $(MATIO_LIBS) -c $(RELEASE_FLAGS) $(FC_FLAGS) pes_m.f90

pesm-debug: aputils smtools-debug
	cd ./src/; \
	$(MPIF90) $(MPIF_INCLUDE) $(MATIO_LIBS) -c $(DEBUG_FLAGS) $(FC_FLAGS) pes_m.f90

# mumps solver routines
pesmc:
	cd ./src/; \
	$(MPIF90) $(MPIF_INCLUDE) $(MATIO_LIBS) -c $(RELEASE_FLAGS) $(FC_FLAGS) pes_mc.f90

pesmc-debug:
	cd ./src/; \
	$(MPIF90) $(MPIF_INCLUDE) $(MATIO_LIBS) -c $(DEBUG_FLAGS) $(FC_FLAGS) pes_mc.f90

pesmd:
	cd ./src/; \
	$(MPIF90) $(MPIF_INCLUDE) $(MATIO_LIBS) -c $(RELEASE_FLAGS) $(FC_FLAGS) pes_md.f90

pesmd-debug:
	cd ./src/; \
	$(MPIF90) $(MPIF_INCLUDE) $(MATIO_LIBS) -c $(DEBUG_FLAGS) $(FC_FLAGS) pes_md.f90

# clean

clean:
	rm -f ./lib/*.a
	rm -f ./src/*.o


# library files

lib: pesmc pesmd pesm
	mkdir -p lib
	rm -f ./lib/libpes.a
	ar rcs ./lib/libpes.a ./src/pes_m.o ./src/pes_mc.o ./src/pes_md.o ./src/sm_tools.o ./src/ap_utils.o ./src/sorting.o

lib-debug: pesmc-debug pesmd-debug pesm-debug
	mkdir -p libautopes
	rm -f ./lib/libpes-debug.a
	ar rcs ./lib/libpes-debug.a ./src/pes_m.o ./src/pes_mc.o ./src/pes_md.o ./src/sm_tools.o ./src/ap_utils.o ./src/sorting.o

	
# centralized solver routines

autopes_mc: lib getoptions
	cd ./src/; \
	$(MPIF90) $(RELEASE_FLAGS) $(FC_FLAGS) $(MPIF_INCLUDE) autopes_mc.f90 matrixLoader.f90 matrixCreator.f90 getoptions.o ../lib/libpes.a -o ../bin/autopes_mc $(ALL_LIBS)

autopes_mc-debug: lib-debug getoptions
	cd ./src/; \
	$(MPIF90) $(DEBUG_FLAGS) $(FC_FLAGS) $(MPIF_INCLUDE) autopes_mc.f90 matrixLoader.f90 matrixCreator.f90 getoptions.o ../lib/libpes-debug.a -o ../bin/autopes_mc $(ALL_LIBS)
	
# distributed solver routines



autopes_md: lib getoptions
	cd ./src/; \
	$(MPIF90) $(RELEASE_FLAGS) $(FC_FLAGS) $(MPIF_INCLUDE) autopes_md.f90 matrixLoader.f90 matrixCreator.f90 getoptions.o ../lib/libpes.a -o ../bin/autopes_md $(ALL_LIBS)

autopes_md-debug: lib-debug getoptions
	cd ./src/; \
	$(MPIF90) $(DEBUG_FLAGS) $(FC_FLAGS) $(MPIF_INCLUDE) autopes_md.f90 matrixLoader.f90 matrixCreator.f90 getoptions.o ../lib/libpes-debug.a -o ../bin/autopes_md $(ALL_LIBS)

default: autopes_mc