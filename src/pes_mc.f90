module pes_mc

    use mpi
    use sm_tools
    use pes_m

    implicit none

    contains

    subroutine pes_mc_init(mumpsPar, MPI_COMM_SOLVER, nZ, nY, nZ_disp, dZ, dY, flagMatrix, tfile)
        implicit none

        type (DMUMPS_STRUC), intent(inout):: mumpsPar

        integer, intent(in) :: MPI_COMM_SOLVER
        integer, intent(in) :: nZ, nY, nZ_disp
        real*8, intent(in) :: dZ, dY
        integer, dimension(nZ, nY), intent(inout) :: flagMatrix
        character*120, intent(in) :: tfile

        write (*,*) '*** PES *** Starting MUMPS'
        call pes_mc_start(mumpsPar, MPI_COMM_SOLVER)
        write (*,*) '*** PES *** Defining the system'
        call pes_mc_define(mumpsPar, nZ, nY, nZ_disp, dZ, dY, flagMatrix, .false., tfile)

    end subroutine pes_mc_init

    subroutine pes_mc_start(mumpsPar, MPI_COMM_SOLVER)

        implicit none

        type (DMUMPS_STRUC), intent(inout):: mumpsPar
        integer, intent(in) :: MPI_COMM_SOLVER

        integer :: proc_no, ierr


        call MPI_COMM_RANK(MPI_COMM_SOLVER, proc_no, ierr)
        write (*,*) '*** PES *** MPI_COMM_SOLVER rank', proc_no

        mumpsPar%JOB = -1
        mumpsPar%SYM = 0
        mumpsPar%PAR = 1
        mumpsPar%COMM = MPI_COMM_SOLVER

        write (*,*) '*** PES *** MUMPS initialisation started'
        call dmumps(mumpsPar)

        write (*,*) '*** PES *** mumpsPar%INFOG(1)', mumpsPar%INFOG(1)
        write (*,*) '*** PES *** mumpsPar%INFOG(2)', mumpsPar%INFOG(2)
        write (*,*) '*** PES *** mumpsPar%INFO(1)', mumpsPar%INFO(1)
        write (*,*) '*** PES *** mumpsPar%INFO(2)', mumpsPar%INFO(2)

        write (*,*) '*** PES *** MUMPS initialized at rank', mumpsPar%myId

        mumpsPar%ICNTL(28) = 1
        mumpsPar%ICNTL(14) = 100
        mumpsPar%ICNTL(7) = 2

        ! comment these lines for debug output
        mumpsPar%ICNTL(1) = -1
        mumpsPar%ICNTL(2) = -1
        mumpsPar%ICNTL(3) = -1
        mumpsPar%ICNTL(4) = -1

        write (*,*) '*** PES *** MUMPS initialisation completed (silent)'
    end subroutine pes_mc_start


    subroutine pes_mc_define(mumpsPar, nZ, nY, nZ_disp, dZ, dY, flagMatrix, reset, tfile)

        implicit none

        type (DMUMPS_STRUC), intent(inout):: mumpsPar

        integer, intent(in) :: nZ, nY, nZ_disp
        real*8, intent(in) :: dZ, dY
        integer, dimension(nZ, nY), intent(inout) :: flagMatrix

        logical, intent(in) :: reset
        character*120, intent(in) :: tfile

        integer :: elementCount
        integer :: matrixDimension
        integer :: ierr

        integer :: z, y, i, bottom, left, right, top, center, nYAdjusted

        integer, dimension(:), allocatable :: testIRN, testJCN
        real*8, dimension(:), allocatable :: testA

        integer*8 :: timeStart, timeEnd
        real*8 :: timeDiff

        real*8 :: hzy, hzySquared, centerValue, sideValue, sideValue2
        integer :: proc_no

        write (*,*) '*** PES *** System definition started.', mumpsPar%myId, mumpsPar%COMM

        hzy = dz / dy
        hzySquared = hzy * hzy

        centerValue = -2.0 * (1.0 + hzySquared)
        sideValue = 1.0 * hzySquared
        sideValue2 = 2.0 * hzySquared

        nYAdjusted = nY - 1



        if (reset) then
            write(*,*) "*** PES ***  MUMPS reset required."
            call pes_mc_stop(mumpsPar)
            call pes_mc_start(mumpsPar, mumpsPar%COMM)
        endif

        call MPI_COMM_RANK(mumpsPar%COMM, proc_no, ierr)
        write(*,*) "*** PES ***  mumpsPar%COMM, proc_no, ierr", mumpsPar%COMM, proc_no, ierr

        matrixDimension = nZ * nYAdjusted

        if (mumpsPar%myId == 0 .or. proc_no == 0) then

            call sm_count(nZ, nYAdjusted, flagMatrix(1:nZ,1:nYAdjusted), elementCount)

            mumpsPar%NZ = elementCount
            mumpsPar%N = matrixDimension

            write(*,*) "mumpsPar%NZ", mumpsPar%NZ
            write(*,*) "mumpsPar%N", mumpsPar%N

            allocate(mumpsPar%IRN(mumpsPar%NZ))
            allocate(mumpsPar%JCN(mumpsPar%NZ))
            allocate(mumpsPar%A(mumpsPar%NZ))

            allocate(mumpsPar%RHS(mumpsPar%N))

            mumpsPar%IRN = 0
            mumpsPar%JCN = 0
            mumpsPar%A = 0
            mumpsPar%RHS = 1

            !call sm_create(nZ, nYAdjusted, nZ_disp, flagMatrix(1:nZ,1:nYAdjusted), elementCount, dZ, dY, mumpsPar%IRN, mumpsPar%JCN, mumpsPar%A)
            call sm_createdist2(nZ, nYAdjusted, nZ_disp, 1, nZ, 1, nYAdjusted, flagMatrix(1:nZ,1:nYAdjusted), dZ, dY, mumpsPar%NZ, mumpsPar%IRN, mumpsPar%JCN, mumpsPar%A)


            !call sm_dump_matrix(tfile, nZ, nYAdjusted, nZ_disp, flagMatrix(1:nZ,1:nYAdjusted), elementCount, dZ, dY, mumpsPar%IRN, mumpsPar%JCN, mumpsPar%A)
        endif

        call pes_sync()

        mumpsPar%JOB = 4
        call dmumps(mumpsPar)

    end subroutine pes_mc_define

    subroutine pes_mc_stop(mumpsPar)
        implicit none

        ! solver
        type (DMUMPS_STRUC), intent(inout):: mumpsPar

        if (mumpsPar%MYID .eq. 0 )then
            deallocate(mumpsPar%IRN)
            deallocate(mumpsPar%JCN)
            deallocate(mumpsPar%A)
            deallocate(mumpsPar%RHS)
        endif

        mumpsPar%JOB = -2
        call dmumps(mumpsPar)

        write(*,*) '*** PES *** Stopping MUMPS'

    end subroutine pes_mc_stop

end module pes_mc
