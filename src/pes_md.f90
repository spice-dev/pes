module pes_md

    use mpi
	use ap_utils
    use sm_tools
    use pes_m

    implicit none

    contains

    subroutine pes_md_init(mumpsPar, MPI_COMM_SOLVER, nZ, nY, nZ_disp, dZ, dY, flagMatrix)
        implicit none

        type (DMUMPS_STRUC), intent(inout):: mumpsPar

        integer, intent(in) :: MPI_COMM_SOLVER
        integer, intent(in) :: nZ, nY, nZ_disp
        real*8, intent(in) :: dZ, dY
        integer, dimension(nZ, nY), intent(inout) :: flagMatrix

        write (*,*) '*** PES *** Starting MUMPS'
        call pes_md_start(mumpsPar, MPI_COMM_SOLVER)
        write (*,*) '*** PES *** Defining the system'
        call pes_md_define(mumpsPar, nZ, nY, nZ_disp, dZ, dY, flagMatrix, .false.)

    end subroutine pes_md_init

    subroutine pes_md_start(mumpsPar, MPI_COMM_SOLVER)

        implicit none

        type (DMUMPS_STRUC), intent(inout):: mumpsPar
        integer, intent(in) :: MPI_COMM_SOLVER


        mumpsPar%JOB = -1
        mumpsPar%SYM = 0
        mumpsPar%PAR = 1
        mumpsPar%COMM = MPI_COMM_SOLVER

        write (*,*) '*** PES *** MUMPS initialisation started'
        call dmumps(mumpsPar)

        ! silence outputs
        mumpsPar%ICNTL(14) = 100
        mumpsPar%ICNTL(1) = -1
        mumpsPar%ICNTL(2) = -1
        mumpsPar%ICNTL(3) = -1
        mumpsPar%ICNTL(4) = -1

		! parallelized input
		mumpsPar%ICNTL(18) = 3
        mumpsPar%ICNTL(7) = 2

        write (*,*) '*** PES *** MUMPS initialisation completed'

    end subroutine pes_md_start


    subroutine pes_md_define(mumpsPar, nZ, nY, nZ_disp, dZ, dY, flagMatrix, reset)


        implicit none

        type (DMUMPS_STRUC), intent(inout):: mumpsPar

        integer, intent(in) :: nZ, nY, nZ_disp
        real*8, intent(in) :: dZ, dY
        integer, dimension(nZ, nY), intent(inout) :: flagMatrix

        logical, intent(in) :: reset

        integer :: elementCount
        integer :: matrixDimension
        integer :: commsize, myrank, ierr

        integer :: z, y, i, bottom, left, right, top, center, nYAdjusted

        integer, dimension(:), allocatable :: zMinAll, zMaxAll, yMinAll, yMaxAll

        integer*8 :: timeStart, timeEnd
        real*8 :: timeDiff

        real*8 :: hzy, hzySquared, centerValue, sideValue, sideValue2

        hzy = dz / dy
        hzySquared = hzy * hzy

        centerValue = 2.0 * (1.0 + hzySquared)
        sideValue = -1.0 * hzySquared
        sideValue2 = - 2.0 * hzySquared

        nYAdjusted = nY - 1

        if (reset) then
            call pes_md_stop(mumpsPar)
            call pes_md_start(mumpsPar, mumpsPar%COMM)
        endif

        call MPI_COMM_SIZE(mumpsPar%COMM, commsize, ierr)
        call MPI_COMM_RANK(mumpsPar%COMM, myrank, ierr)

        allocate(zMinAll(0:commsize - 1))
        allocate(zMaxAll(0:commsize - 1))
        allocate(yMinAll(0:commsize - 1))
        allocate(yMaxAll(0:commsize - 1))

        call sm_sliceflag(nZ, nYAdjusted, commsize, zMinAll, zMaxAll, yMinAll, yMaxAll)

		write (*,*) 'zMinAll', zMinAll
		write (*,*) 'zMaxAll', zMaxAll
		write (*,*) 'yMinAll', yMinAll
		write (*,*) 'yMaxAll', yMaxAll

        matrixDimension = nZ * nYAdjusted

        !read (inputUnitMatrix,*) mumpsPar%N, mumpsPar%NZ, mumpsPar%NZ_loc

		write (*,*) 'mumpsPar%myId', mumpsPar%myId
		write (*,*) 'myrank', myrank

        if (mumpsPar%myId == 0 .or. myrank == 0) then

            mumpsPar%N = matrixDimension
            call sm_count(nZ, nYAdjusted, flagMatrix(1:nZ,1:nYAdjusted), mumpsPar%NZ)

            !allocate(mumpsPar%IRN(mumpsPar%NZ))
            !allocate(mumpsPar%JCN(mumpsPar%NZ))
            allocate(mumpsPar%RHS(mumpsPar%N))

            !mumpsPar%IRN = 0
            !mumpsPar%JCN = 0
            mumpsPar%RHS = 0.0

        endif

        call sm_countdist2(nZ, nYAdjusted, zMinAll(myrank), zMaxAll(myrank), yMinAll(myrank), yMaxAll(myrank), flagMatrix(1:nZ,1:nYAdjusted), mumpsPar%NZ_loc)

        allocate(mumpsPar%IRN_loc(mumpsPar%NZ_loc))
        allocate(mumpsPar%JCN_loc(mumpsPar%NZ_loc))
        allocate(mumpsPar%A_loc(mumpsPar%NZ_loc))

        call sm_createdist2(nZ, nYAdjusted, nZ_disp, zMinAll(myrank), zMaxAll(myrank), yMinAll(myrank), yMaxAll(myrank), flagMatrix(1:nZ,1:nYAdjusted), dZ, dY, mumpsPar%NZ_loc, mumpsPar%IRN_loc, mumpsPar%JCN_loc, mumpsPar%A_loc)

		!write (*,*) 'Indices i'
		!call printIntegerMatrix(mumpsPar%NZ_loc, 1, mumpsPar%IRN_loc)
		!write (*,*) 'Indices j'
		!call printIntegerMatrix(mumpsPar%NZ_loc, 1, mumpsPar%JCN_loc)
		!write (*,*) 'Values'
		!call printRealMatrix(mumpsPar%NZ_loc, 1, mumpsPar%A_loc)

        call pes_sync()

        mumpsPar%JOB = 4
        call dmumps(mumpsPar)


        deallocate(zMinAll)
        deallocate(zMaxAll)
        deallocate(yMinAll)
        deallocate(yMaxAll)

    end subroutine pes_md_define


    subroutine pes_md_stop(mumpsPar)
        implicit none
        ! solver
        type (DMUMPS_STRUC), intent(inout):: mumpsPar

        if (mumpsPar%MYID .eq. 0 )then
            !deallocate(mumpsPar%IRN)
            !deallocate(mumpsPar%JCN)
            deallocate(mumpsPar%RHS)
        endif

        deallocate(mumpsPar%IRN_loc)
        deallocate(mumpsPar%JCN_loc)
        deallocate(mumpsPar%A_loc)


        mumpsPar%JOB = -2
        call dmumps(mumpsPar)


    end subroutine pes_md_stop

end module pes_md
