
program autopes_mc
    use getoptions
    use mpi
    use sm_tools
    use pes_mc
    use IFPORT

	implicit none

    character :: okey
    character*2048 :: inputFile, outputFile

    type (DMUMPS_STRUC) :: mumpsPar

    integer :: nZ, nY, z, y
    real*8 :: dZ, dY
    integer :: n0
    integer, dimension(:,:), allocatable :: flagMatrix
    real*8, dimension(:,:), allocatable :: potentialMatrix, potentialMatrix2, densityMatrix, densityMatrix2

    integer :: mympi_commsize, mympi_rank, mympi_ierr, mympi_provided_threading

    integer :: elementCount, matrix_dimension
    integer*8 :: time1, time2, time_diff

    integer, parameter :: steps = 10
	real*8, dimension(steps) :: stats

    integer :: i, bc
    real*8 :: one, init_time

    character*120 :: tfile

    integer*8, dimension(:,:), allocatable :: mem_usage, vm_size
    integer*8, dimension(:), allocatable :: mem_usage_step, vm_size_step

    tfile = 'test';
    bc = 1;
    call srand(1234)

    do
        okey=getopt('o:y:z:mn')

        if(okey.eq.'o') then
            outputFile=trim(optarg)
            write(*,*) 'The output will be: ', trim(outputFile)
        end if

        if(okey.eq.'y') then
            read (optarg,*) nY
            write(*,*) 'Size in y: ', nY
        end if

        if(okey.eq.'z') then
            read (optarg,*) nZ
            write(*,*) 'Size in z: ', nZ
        end if

        if(okey.eq.'>') exit

    enddo

	call MPI_INIT_THREAD(MPI_THREAD_MULTIPLE, mympi_provided_threading, mympi_ierr)
    write(*,*) 'MPI Threaded init', MPI_THREAD_MULTIPLE, mympi_provided_threading

	call MPI_COMM_SIZE(MPI_COMM_WORLD, mympi_commsize, mympi_ierr)
	call MPI_COMM_RANK(MPI_COMM_WORLD, mympi_rank, mympi_ierr)

    if (mympi_rank == 0) then
        dY = 1
        dZ = 1
        n0 = 50
    endif

    allocate(mem_usage(steps + 1, mympi_commsize))
	allocate(mem_usage_step(mympi_commsize))
	allocate(vm_size(steps + 1, mympi_commsize))
	allocate(vm_size_step(mympi_commsize))
	mem_usage = 0
	mem_usage_step = 0
	vm_size = 0
	vm_size_step = 0

    call MPI_BCAST(nY, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(nZ, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(dY, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(dZ, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, mympi_ierr)
    call MPI_BCAST(n0, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, mympi_ierr)

    write (*,*) 'Dimensions loaded and distributed.', nZ, nY, dZ, dY, n0

    matrix_dimension = nY * nZ
    allocate(flagMatrix(nZ, nY))
    allocate(potentialMatrix(nZ, nY))
    allocate(potentialMatrix2(nZ, nY))
    allocate(densityMatrix(nZ, nY))
    allocate(densityMatrix2(nZ, nY))

    if (mympi_rank == 0) then
        call createFlag(nZ, nY, bc, flagMatrix)
        call createPlasma(nZ, nY, bc, flagMatrix, potentialMatrix, densityMatrix)
    endif
    call MPI_BARRIER(MPI_COMM_WORLD, mympi_ierr)

    write (*,*) 'Distributing matrices.', matrix_dimension
    call MPI_BCAST(flagMatrix, matrix_dimension, MPI_INTEGER, 0, MPI_COMM_WORLD, mympi_ierr)
    write (*,*) 'flagMatrix loaded and distributed.'
    call MPI_BCAST(potentialMatrix, matrix_dimension, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, mympi_ierr)
    write (*,*) 'potentialMatrix loaded and distributed.'
    call MPI_BCAST(densityMatrix, matrix_dimension, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, mympi_ierr)
    write (*,*) 'densityMatrix loaded and distributed.'


    call gettime(time1)
    !call pes3d_init(mumpsPar, MPI_COMM_WORLD, nX, nZ, nY, dX, dZ, dY, flagMatrix)

    call pes_mc_init(mumpsPar, MPI_COMM_WORLD, nZ, nY, 0, dZ, dY, flagMatrix, tfile)

    call gettime(time2)
    time_diff = time2 - time1
    write (*,*) '#time_diff;init;', time_diff
    init_time = 1.0 * time_diff
    call get_cluster_memory_usage_kb(mem_usage_step, vm_size_step, mympi_commsize)
    mem_usage(1,:) = mem_usage_step(:)
    vm_size(1,:) = vm_size_step(:)

    one = 1
    densityMatrix2 = densityMatrix
    do i = 1, steps

        do y = 1, nY
            do z = 1, nZ
                if (flagMatrix(z, y) == 1) then
                    densityMatrix2(z, y) = densityMatrix2(z, y) + 50 * (0.05 * (rand() - 0.5))
                endif
            enddo
        enddo
        !densityMatrix2 = 0
        write (*,*) 'densityMatrix2 sum', sum(densityMatrix2)

        one = - one
        call gettime(time1)

        call pes_solve(mumpsPar, nZ, nY, 0, dZ, dY, n0, flagMatrix, potentialMatrix, densityMatrix2, potentialMatrix2, i, tfile)
        call gettime(time2)
        time_diff = time2 - time1
        write (*,*) '#time_diff;solve;', time_diff
        stats(i) = 1.0 * time_diff

        call get_cluster_memory_usage_kb(mem_usage_step, vm_size_step, mympi_commsize)
        mem_usage(1+i,:) = mem_usage_step(:)
        vm_size(1+i,:) = vm_size_step(:)
    enddo

    if (mympi_rank == 0) then
        call saveProgress(outputFile, nZ, nY, potentialMatrix, potentialMatrix2, densityMatrix2, flagMatrix, steps, stats, init_time, mympi_commsize, mem_usage, vm_size)
    endif

    call pes_mc_stop(mumpsPar)

    !call pes3d_stop(mumpsPar)
    !write (*,*) 'mumps stopped'
    !deallocate(flagMatrix)
    !deallocate(potentialMatrix)
    !deallocate(potentialMatrix2)
    !deallocate(densityMatrix)
    !deallocate(densityMatrix2)
    !write (*,*) 'deallocated'

	call MPI_FINALIZE(mympi_ierr)

end program autopes_mc


