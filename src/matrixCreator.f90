subroutine createFlag(nZ, nY, bc, flagMatrix)
	implicit none

	integer, intent(in):: nZ, nY, bc

	integer, dimension(nZ, nY), intent(out) :: flagMatrix

	integer :: z1, z2, y1, y2

    flagMatrix = 1

    z1 = 1
    z2 = nZ * 0.2
    y1 = 1
    y2 = nY * 0.4

    flagMatrix(z1:z2, y1:y2) = 0

    y1 = nY * 0.6
    y2 = nY

    flagMatrix(z1:z2, y1:y2) = 0

    if (bc == 1) then
        flagMatrix(1, 1:nY) = 0
        flagMatrix(nZ, 1:nY) = 0
    elseif (bc == 2) then
        z1 = nZ * 0.6

        flagMatrix(1, 1:nY) = 0
        flagMatrix(z1, 1:nY) = 0
        flagMatrix(nZ, 1:nY) = 2
    endif

    ! comment this for old testing setup
    flagMatrix = 1

    if (nY < 10 .or. nZ < 10) then
        flagMatrix(1, 1) = 0
        flagMatrix(nZ, nY) = 0
    else
        ! boundaries
        flagMatrix(1, 1:nY) = 0
        flagMatrix(nZ, 1:nY) = 0

        ! flat head
        z1 = nZ * 0.1
        z1 = 11
        flagMatrix(1:z1, 1:nY) = 0

        ! probe pin
        z1 = 1
        z2 = nZ * 0.25
        y1 = 0.45 * nY
        y2 = 0.55 * nY
        !flagMatrix(z1:z2,y1:y2) = 0

    endif
    ! comment this ^ for old testing setup

end subroutine createFlag

subroutine createPlasma(nZ, nY, bc, flagMatrix, potentialMatrix, densityMatrix)
	use matio
    use IFPORT
	implicit none

	integer, intent(in):: nZ, nY, bc
	integer, dimension(nZ, nY), intent(in) :: flagMatrix

	real*8, dimension(nZ, nY), intent(out) :: potentialMatrix, densityMatrix

	integer :: z1, z2, y1, y2, y, z, delta
	real*8 :: l_r

    call srand(1234)

	delta = 2;
	densityMatrix = 0;

	do z = 1, nZ
        do y = 1, nY
            if (flagMatrix(z, y) == 1) then
                densityMatrix(z, y) = (0.5 * rand() - 0.25)
            endif
        enddo
    enddo

	!densityMatrix = 0;
	potentialMatrix = 0

    z1 = 1
    z2 = nZ * 0.2
    y1 = 1
    y2 = nY * 0.4

    potentialMatrix(z1:z2, y1:y2) = -3

    y1 = nY * 0.6
    y2 = nY

    potentialMatrix(z1:z2, y1:y2) = -3

    if (bc == 1) then
        potentialMatrix(1, 1:nY) = -3
        potentialMatrix(nZ, 1:nY) = 0
    elseif (bc == 2) then
        potentialMatrix(1, 1:nY) = -3
    endif

    ! comment this for old testing setup
    l_r = 0.01 * nZ
    do z = 1, nZ
        do y = 1, nY
            if (flagMatrix(z, y) == 1) then
                densityMatrix(z, y) = 50 * (0.8 * dexp(0.5 * (-z + 11) / l_r) + (0.8 * (rand() - 0.5)))
            endif
        enddo
    enddo

    potentialMatrix = 0
    if (nY < 10 .or. nZ < 10) then
        potentialMatrix(1, 1) = 3
        potentialMatrix(nZ, nY) = -3
    else
        ! boundaries
        potentialMatrix(nZ, 1:nY) = 0
        potentialMatrix(1, 1:nY) = -3

        ! flat head
        z1 = nZ * 0.1
        z1 = 11
        potentialMatrix(1:z1, 1:nY) = -3

        ! probe pin

        z1 = 1
        z2 = nZ * 0.25
        y1 = 0.45 * nY
        y2 = 0.55 * nY
        !potentialMatrix(z1:z2,y1:y2) = -10

    endif
    ! comment this ^ for old testing setup

end subroutine createPlasma
