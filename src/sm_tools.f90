module sm_tools

    use ap_utils
    use sorting
    use mpi

    implicit none

contains

    subroutine sm_assemble(nZ, nY, dZ, dY, flag_matrix, stencil_mapping, matrix_data, element_count)

        implicit none

        integer, intent(in) :: nZ, nY
        real*8, intent(in) :: dZ, dY
        integer, dimension(nZ, nY), intent(in) :: flag_matrix

        integer, dimension(7, nZ * nY), intent(out) ::  stencil_mapping
        real*8, dimension(5, nZ * nY), intent(out) ::  matrix_data

        integer, intent(out) :: element_count

        real*8 :: md_center, md_side, zy_ratio_squared
        integer :: z, y, m_i, flag_value


        stencil_mapping = 0
        matrix_data = 0
        element_count = 0

        zy_ratio_squared = (dZ * dZ) / (dY * dY)

        md_center = -2.0 * (1.0 + zy_ratio_squared)
        md_side = 1.0 * zy_ratio_squared

        m_i = 1;

        do z = 1, nZ
            do y = 1, nY
                stencil_mapping(1, m_i) = z;
                stencil_mapping(2, m_i) = y;
                flag_value = flag_matrix(z, y)
                call sm_get_stencil(z, y, Nz, Ny, flag_value, md_center, md_side, stencil_mapping(3:7, m_i), matrix_data(1:5,m_i))

                element_count = element_count + sum(stencil_mapping(3:7, m_i))
                m_i = m_i + 1;
            enddo
        enddo

    end subroutine sm_assemble

    subroutine sm_get_stencil(z, y, n_z, n_y, flag_value, md_center, md_side, stencil, stencil_data)

        implicit none

        integer, intent(in) :: z, y, n_z, n_y
        integer, intent(in) :: flag_value
        real*8, intent(in)  :: md_center, md_side

        integer, dimension(5), intent(out) ::  stencil
        real*8, dimension(5), intent(out) ::  stencil_data

        integer :: current_element_count

        ! top left center right bottom
        stencil = (/ 0, 0, 1, 0, 0/)
        stencil_data = (/ 0, 0, 1, 0, 0/)
        current_element_count = 1

        if (flag_value == 0) then
            ! object
            stencil = (/ 0, 0, 1, 0, 0/)
            stencil_data = (/ 0.0, 0.0, 1.0, 0.0, 0.0/)

        elseif (flag_value == 2 .and. z == n_z) then
            ! zero e field on top
            stencil = (/ 0, 1, 1, 1, 1/)
            stencil_data = (/ real*8 :: 0.0, 2.0 * md_side, md_center, md_side, md_side/)
        elseif (flag_value == 3 .and. z == 1) then
            ! zero e field on top
            stencil = (/ 1, 1, 1, 1, 0/)
            stencil_data = (/ real*8 :: md_side, md_side, md_center, 2.0 * md_side,  0.0/)
        else
            ! plasma
            stencil = (/ 1, 1, 1, 1, 1/)
            stencil_data = (/ md_side, md_side, md_center, md_side, md_side/)
        endif


    end subroutine sm_get_stencil


    subroutine sm_count(nZ, nY, flagMatrix, elementCount)

        implicit none

        integer, intent(in) :: nZ, nY
        integer, dimension(nZ, nY), intent(in) :: flagMatrix
        integer, intent(out) :: elementCount

        integer :: z, y, flagCount

        elementCount = 0
        flagCount = 0

        do z = 1, nZ
            do y = 1, nY
                call sm_flagCount(flagMatrix(z, y), flagCount)
                elementCount = elementCount + flagCount
            enddo
        enddo

    end subroutine sm_count

    subroutine sm_countdist(zMin, zMax, yMin, yMax, flagMatrixLoc, elementCount)

        implicit none

        integer, intent(in) :: zMin, zMax, yMin, yMax
        integer, dimension(zMin:zMax, yMin:yMax), intent(in) :: flagMatrixLoc
        integer, intent(out) :: elementCount

        integer :: z, y, flagCount

        elementCount = 0
        flagCount = 0

        do z = zMin, zMax
            do y = yMin, yMax
                call sm_flagCount(flagMatrixLoc(z, y), flagCount)
                elementCount = elementCount + flagCount
            enddo
        enddo

    end subroutine sm_countdist

    subroutine sm_countdist2(nZ, nY, zMin, zMax, yMin, yMax, flagMatrix, elementCount)

        implicit none

        integer, intent(in) :: nZ, nY, zMin, zMax, yMin, yMax
        integer, dimension(nZ, nY), intent(in) :: flagMatrix
        integer, intent(out) :: elementCount

        integer :: z, y, flagCount

        elementCount = 0
        flagCount = 0

        do z = zMin, zMax
            do y = yMin, yMax
                call sm_flagCount(flagMatrix(z, y), flagCount)
                elementCount = elementCount + flagCount
            enddo
        enddo

    end subroutine sm_countdist2

    subroutine sm_flagCount(flag, flagCount)
        implicit none

        integer, intent(in) :: flag
        integer, intent(out) :: flagCount

        flagCount = 0

        if (flag == 0) then
            flagCount = 1
        else if (flag == 1) then
            flagCount = 5
        else if (flag == 2 .or. flag == 3) then
            flagCount = 4
        else
            flagCount = 5
        endif

    end subroutine sm_flagCount

    subroutine sm_resetflags(nZ, nY, flagMatrix)

        implicit none

        integer, intent(in) :: nZ, nY
        integer, dimension(nZ, nY), intent(inout) :: flagMatrix

        integer :: z, y

        do z = 1, nZ
            do y = 1, nY
                if (flagMatrix(z, y) .ne. 0) then
                    flagMatrix(z, y) = 1
                endif
            enddo
        enddo

    end subroutine sm_resetflags

    subroutine sm_ijx2csr(elementCount, nRows, Ai, Aj, Ax, Bi, Bp, Bx)

        implicit none

        integer, intent(in) :: elementCount, nRows
        integer, dimension(elementCount), intent(in) :: Ai, Aj
        real*8, dimension(elementCount), intent(in) :: Ax

        integer, dimension(elementCount), intent(out) :: Bi
        integer, dimension(nRows + 1), intent(out) :: Bp
        real*8, dimension(elementCount), intent(out) :: Bx

        integer :: i, previousRow, rowIndex
        integer, dimension(elementCount, 3) :: A

        A = 0;

        do i = 1, elementCount
            A(i, 1) = Ai(i);
            A(i, 2) = Aj(i);
            A(i, 3) = i;
        enddo

        Bi = 0
        Bp = 0
        Bx = 0.0

        Bp(1) = 1

        if (elementCount == 1) then
            return
        endif

        call mergesort_int(elementCount, 3, A, 2, (/1, 2/), 1)

        previousRow = A(1, 1);

        do i = 2, elementCount
            if (A(i, 1) .ne. previousRow) then
                rowIndex = rowIndex + 1
                Bp(rowIndex) = i - 1
                previousRow = A(i, 1);
            endif
        enddo

        Bp(nRows + 1) = elementCount + 1
        Bi = A(:,1);
        Bx = Ax(A(:,3));

    end subroutine sm_ijx2csr


    subroutine sm_create(nZ, nY, nZ_disp, flagMatrix, elementCount, dZ, dY, Ai, Aj, Ax)
        implicit none

        !integer sm_linearIndex

        integer, intent(in) :: nZ, nY, nZ_disp
        integer, dimension(nZ, nY), intent(in) :: flagMatrix
        integer, intent(in) :: elementCount
        real*8, intent(in) :: dY, dZ

        integer, dimension(elementCount), intent(out) :: Ai, Aj
        real*8, dimension(elementCount), intent(out) :: Ax

        integer :: z, y, i
        integer :: left, top, right, bottom, center
        real*8 :: hzy, hzySquared, centerValue, sideValue, sideValue2

        hzy = dZ / dY
        hzySquared = hzy * hzy
        i = 1

        ! this is correct
        centerValue = 2.0 * (1.0 + hzySquared)
        sideValue = -1.0 * hzySquared
        sideValue2 = -2.0 * hzySquared


        Ai = 0
        Aj = 0
        Ax = 0.0

        do z = 1, nZ
            do y = 1, nY

                call sm_setijx(nZ, nY, nZ_disp, centerValue, sideValue, sideValue2, z, y, flagMatrix(z, y), elementCount, Ai, Aj, Ax, i)

            enddo
        enddo
    end subroutine sm_create

    subroutine sm_sliceflag(nZ, nY, commsize, zMinAll, zMaxAll, yMinAll, yMaxAll)
        implicit none
        integer, intent(in) :: nZ, nY, commsize
        integer, dimension(0:commsize - 1), intent(out) :: zMinAll, zMaxAll, yMinAll, yMaxAll

        integer :: rankindex, delta, z, y

        ! we do the slicing only one direction
        zMinAll = 1
        zMaxAll = nZ


        ! and now slice other
        delta = nY / commsize

        y = 0

        do rankindex = 0, commsize - 1

            yMinAll(rankindex) = y + 1
            yMaxAll(rankindex) = y + delta

            y = y + delta

        enddo

        yMaxAll(commsize - 1) = nY

    end subroutine sm_sliceflag

    subroutine sm_distint2d(nZ, nY, int2d, MPI_COMM, mpirank, transactionId, commsize, zMin, zMax, yMin, yMax, int2dLoc)

        implicit none

        integer, intent(in) :: nZ, nY
        integer, dimension(nZ, nY), intent(in) :: int2d
        integer, intent(in) :: MPI_COMM, mpirank, commsize, transactionId
        integer, dimension(0:commsize - 1), intent(in) :: zMin, zMax, yMin, yMax

        integer, dimension(zMin(mpirank):zMax(mpirank), yMin(mpirank):yMax(mpirank)), intent(inout) :: int2dLoc

        integer :: ierr
        integer, dimension(MPI_STATUS_SIZE) :: mpistatus
        integer :: dimZ, dimY, elementCount
        integer :: targetRank, mpiId

        if (mpirank == 0) then
            do targetRank = 1, commsize - 1
                dimZ = zMax(targetRank) - zMin(targetRank) + 1
                dimY = yMax(targetRank) - yMin(targetRank) + 1
                elementCount = dimZ * dimY
                call MPI_SEND(int2d(zMin(targetRank):zMax(targetRank), yMin(targetRank):yMax(targetRank)), elementCount, MPI_INTEGER, targetRank, transactionId, MPI_COMM, ierr)
            enddo
            int2dLoc(zMin(0):zMax(0), yMin(0):yMax(0)) = int2d(zMin(0):zMax(0), yMin(0):yMax(0))
        else
            dimZ = zMax(mpirank) - zMin(mpirank) + 1
            dimY = yMax(mpirank) - yMin(mpirank) + 1
            elementCount = dimZ * dimY

            call MPI_RECV(int2dLoc(zMin(mpirank):zMax(mpirank), yMin(mpirank):yMax(mpirank)), elementCount, MPI_INTEGER, 0, transactionId, MPI_COMM, mpistatus, ierr)

        endif

    end subroutine sm_distint2d

    subroutine sm_distreal2d(nZ, nY, real2d, MPI_COMM, mpirank, transactionId, commsize, zMin, zMax, yMin, yMax, real2dLoc)

        implicit none

        integer, intent(in) :: nZ, nY
        real*8, dimension(nZ, nY), intent(in) :: real2d
        integer, intent(in) :: MPI_COMM, mpirank, commsize, transactionId
        integer, dimension(0:commsize - 1), intent(in) :: zMin, zMax, yMin, yMax

        real*8, dimension(zMin(mpirank):zMax(mpirank), yMin(mpirank):yMax(mpirank)), intent(inout) :: real2dLoc

        integer :: ierr
        integer, dimension(MPI_STATUS_SIZE) :: mpistatus
        integer :: dimZ, dimY, elementCount
        integer :: targetRank, mpiId

        if (mpirank == 0) then
            do targetRank = 1, commsize - 1
                dimZ = zMax(targetRank) - zMin(targetRank) + 1
                dimY = yMax(targetRank) - yMin(targetRank) + 1
                elementCount = dimZ * dimY
                call MPI_SEND(real2d(zMin(targetRank):zMax(targetRank), yMin(targetRank):yMax(targetRank)), elementCount, MPI_DOUBLE_PRECISION, targetRank, transactionId, MPI_COMM, ierr)
            enddo
            real2dLoc(zMin(0):zMax(0), yMin(0):yMax(0)) = real2d(zMin(0):zMax(0), yMin(0):yMax(0))
        else
            dimZ = zMax(mpirank) - zMin(mpirank) + 1
            dimY = yMax(mpirank) - yMin(mpirank) + 1
            elementCount = dimZ * dimY

            call MPI_RECV(real2dLoc(zMin(mpirank):zMax(mpirank), yMin(mpirank):yMax(mpirank)), elementCount, MPI_DOUBLE_PRECISION, 0, transactionId, MPI_COMM, mpistatus, ierr)

        endif

    end subroutine sm_distreal2d

    subroutine sm_createdist(nZ, nY, nZ_disp, zMin, zMax, yMin, yMax, flagMatrixLoc, dZ, dY, elementCount, Ai, Aj, Ax)
        implicit none

        !integer sm_linearIndex

        integer, intent(in) :: nZ, nY, nZ_disp, zMin, zMax, yMin, yMax
        integer, dimension(zMin:zMax, yMin:yMax), intent(in) :: flagMatrixLoc
        real*8, intent(in) :: dY, dZ
        integer, intent(in) :: elementCount

        integer, dimension(elementCount), intent(out) :: Ai, Aj
        real*8, dimension(elementCount), intent(out) :: Ax

        integer :: z, y, i
        integer :: left, top, right, bottom, center
        real*8 :: hzy, hzySquared, centerValue, sideValue, sideValue2

        hzy = dZ / dY
        hzySquared = hzy * hzy
        i = 1

        ! this is correct
        centerValue = -2.0 * (1.0 + hzySquared)
        sideValue = 1.0 * hzySquared
        sideValue2 = 2.0 * hzySquared



        Ai = 0
        Aj = 0
        Ax = 0.0

        do z = zMin, zMax
            do y = yMin, yMax

                call sm_setijx(nZ, nY, nZ_disp, centerValue, sideValue, sideValue2, z, y, flagMatrixLoc(z, y), elementCount, Ai, Aj, Ax, i)

            enddo
        enddo

    end subroutine sm_createdist

    subroutine sm_setijx(nZ, nY, nZ_disp, centerValue, sideValue, sideValue2, z, y, flagValue, elementCount, Ai, Aj, Ax, i)

        implicit none

        integer, intent(in) :: nZ, nY, nZ_disp
        real*8 :: centerValue, sideValue, sideValue2

        integer, intent(in) :: z, y, flagValue

        integer, intent(in) :: elementCount
        integer, dimension(elementCount), intent(inout) :: Ai, Aj
        real*8, dimension(elementCount), intent(inout) :: Ax
        integer, intent(inout) :: i

        integer :: left, top, right, bottom, center

        center = sm_linearIndex(nZ, nY, z, y)

        if (flagValue > 0) then

            bottom = sm_linearIndex(nZ, nY, z - 1, y)
            top = sm_linearIndex(nZ, nY, z + 1, y)
            left = sm_linearIndex(nZ, nY, z, y - 1)
            right = sm_linearIndex(nZ, nY, z, y + 1)

            if ((nZ_disp .gt. 0).and.(y == 1).and.(z .gt. nZ_disp)) then

                left = sm_linearIndex(nZ, nY, z - nZ_disp, y - 1)

            else if ((nZ_disp .gt. 0).and.(y == nY).and.(z .lt. nZ - nZ_disp)) then

                right = sm_linearIndex(nZ, nY, z + nZ_disp, y + 1)

            endif

            Ai(i) = center
            Aj(i) = left
            Ax(i) = sideValue
            i = i + 1

            Ai(i) = center
            Aj(i) = center
            Ax(i) = centerValue
            i = i + 1

            Ai(i) = center
            Aj(i) = right
            Ax(i) = sideValue
            i = i + 1


            if ((flagValue == 2 .or. flagValue == 3) .and. (z == 1 .or. z == nZ)) then
                ! zero E field on the edge in z direction

                if (z == 1) then
                    ! no bottom point

                    Ai(i) = center
                    Aj(i) = top
                    Ax(i) = sideValue2
                    i = i + 1

                elseif (z == nZ) then
                    ! no top point

                    Ai(i) = center
                    Aj(i) = bottom
                    Ax(i) = sideValue2
                    i = i + 1

                else
                    write (*,*) '*** PES *** Error! Wrong flag value-position combination, possible memory leak.'
                endif

            else
                ! flagMatrix(z, y) == 1 // or others

                ! both points are present

                Ai(i) = center
                Aj(i) = bottom
                Ax(i) = sideValue
                i = i + 1

                Ai(i) = center
                Aj(i) = top
                Ax(i) = sideValue
                i = i + 1

            endif
        else

            Ai(i) = center
            Aj(i) = center
            Ax(i) = 1
            i = i + 1

        endif


    end subroutine sm_setijx

    subroutine sm_createdist2(nZ, nY, nZ_disp, zMin, zMax, yMin, yMax, flagMatrix, dZ, dY, elementCount, Ai, Aj, Ax)
        implicit none

        !integer sm_linearIndex

        integer, intent(in) :: nZ, nY, nZ_disp, zMin, zMax, yMin, yMax
        integer, dimension(nZ, nY), intent(in) :: flagMatrix
        real*8, intent(in) :: dY, dZ
        integer, intent(in) :: elementCount

        integer, dimension(elementCount), intent(out) :: Ai, Aj
        real*8, dimension(elementCount), intent(out) :: Ax

        integer :: z, y, i
        integer :: left, top, right, bottom, center
        real*8 :: hzy, hzySquared, centerValue, sideValue, sideValue2

        hzy = dZ / dY
        hzySquared = hzy * hzy
        i = 1

        ! this is correct
        write(*,*) 'Creating matrix'
        centerValue = -2.0 * (1.0 + hzySquared)
        sideValue = 1.0 * hzySquared
        sideValue2 = 2.0 * hzySquared


        Ai = 0
        Aj = 0
        Ax = 0.0

        do z = zMin, zMax
            do y = yMin, yMax
                call sm_setijx(nZ, nY, nZ_disp, centerValue, sideValue, sideValue2, z, y, flagMatrix(z, y), elementCount, Ai, Aj, Ax, i)
            enddo
        enddo

    end subroutine sm_createdist2

    subroutine sm_rhs(nZ, nY, dZ, dY, n0, flagMatrix, potentialMatrix, densityMatrix, rightHandSide)

        implicit none

        ! input properties
        integer, intent(in) :: nZ, nY
        real*8, intent(in) :: dZ, dY
        integer, intent(in) :: n0
        integer, dimension(nZ, nY), intent(in):: flagMatrix
        real*8, dimension(nZ, nY), intent(in):: potentialMatrix, densityMatrix
        real*8, dimension(nZ * nY), intent(out) :: rightHandSide

        integer :: z, y, matrixIndex
        real*8 :: charge

        integer*8 :: timeStart, timeEnd, timeDiff

        rightHandSide = 0
        matrixIndex = 1

        write(*,*) 'sm_rhs', nZ, nY, dZ, dY, n0, (real(n0) * dZ * dY)

        do z = 1, nZ
            do y = 1, nY
                charge = - dZ * dZ * densityMatrix(z, y) / (real(n0) * dZ * dY)

                ! if (abs(densityMatrix(z, y) > 0) then
                !     rightHandSide(matrixIndex) = -10
                ! else
                !     rightHandSide(matrixIndex) = potentialMatrix(z, y)
                ! endif

                rightHandSide(matrixIndex) = merge(potentialMatrix(z, y), charge, flagMatrix(z, y) .eq. 0)

                !rightHandSide(matrixIndex) = real(flagMatrix(z, y))*charge + (1.0 - real(flagMatrix(z, y)))*potentialMatrix(z, y)

                !write(*,*) z, y
                !write(*,*) flagMatrix(z, y), densityMatrix(z, y), potentialMatrix(z, y)
                !write(*,*) charge, rightHandSide(matrixIndex)


                matrixIndex = matrixIndex + 1
            enddo
        enddo


    end subroutine sm_rhs

    integer function sm_linearIndex(nZ, nY, z, y)

        implicit none
        integer, intent(in) :: nZ, nY, z, y
        integer :: z1, y1

        z1 = mod(z + nZ, nZ)
        y1 = mod(y + nY, nY)

        if (z1 == 0) z1 = nZ
        if (y1 == 0) y1 = nY

        sm_linearIndex = nY * (z1 - 1) + y1

        return

    end function sm_linearIndex

    subroutine sm_result(elementCount, rhs, nZ, nY, nZ_disp, newPotMatrix)
        implicit none

        integer, intent(in) :: elementCount
        real*8, dimension(elementCount), intent(in) :: rhs
        integer, intent(in) :: nZ, nY, nZ_disp
        real*8, dimension(nZ, nY), intent(inout) :: newPotMatrix

        integer :: z, y, matrixIndex

        matrixIndex = 1
        do z = 1, nZ
            do y = 1, nY - 1
                newPotMatrix(z, y) = rhs(matrixIndex)
                matrixIndex = matrixIndex + 1
            enddo
        enddo

        if (nZ_disp == 0) then
            newPotMatrix(1:nZ, nY) = newPotMatrix(1:nZ, 1)
        else
            newPotMatrix(1:nZ - nZ_disp, nY) = newPotMatrix(nZ_disp + 1:nZ, 1)
        endif

        if (nZ_disp .gt. 0) then
            newPotMatrix(nZ - nZ_disp:nZ, nY) = 0;
        endif

    end subroutine sm_result

    subroutine sm_dump_rhs(mat_file, count, nZ, nY, dZ, dY, n0, flagMatrix, potentialMatrix, densityMatrix, rightHandSide)
        USE MATIO

        implicit none

        character*120, intent(in) :: mat_file

        ! input properties
        integer, intent(in) :: count, nZ, nY
        real*8, intent(in) :: dZ, dY
        integer, intent(in) :: n0
        integer, dimension(nZ, nY), intent(in):: flagMatrix
        real*8, dimension(nZ, nY), intent(in):: potentialMatrix, densityMatrix
        real*8, dimension(nZ * nY), intent(out) :: rightHandSide


        character*4:: count_str

        integer :: IERR

        TYPE(MAT_T) :: MAT
        TYPE(MATVAR_T) :: MATVAR
        integer:: use_compression

        use_compression = 0
        count_str = '0000'
        CALL FMAT_LOGINIT('sm_dump_rhs')


        write(count_str(1:4),'(I4.4)') count / 100

        ! defunct - don't know how to include integers in strings
        write (6,*) 'Writing results to ',trim(mat_file)//'-rhs_'//count_str//'.mat'

        ierr = FMAT_CREATE(trim(mat_file)//'-rhs_'//count_str//'.mat'//char(0), MAT)
        ! WRITE FIRST VARIABLE

        ierr = FMAT_VARCREATE('nZ', nZ, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, nZ, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('nY', nY, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, nY, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('dZ', dZ, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, dZ, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('dY', dY, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, dY, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('n0', n0, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, n0, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('flagMatrix', flagMatrix, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, flagMatrix, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('potentialMatrix', potentialMatrix, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, potentialMatrix, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('densityMatrix', densityMatrix, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, densityMatrix, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('rightHandSide', rightHandSide, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, rightHandSide, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_CLOSE(MAT)

    end subroutine sm_dump_rhs

    subroutine sm_dump_result(mat_file, count, nZ, nY, dZ, dY, n0, flagMatrix, potentialMatrix, densityMatrix, rightHandSide)
        USE MATIO

        implicit none

        character*120, intent(in) :: mat_file

        ! input properties
        integer, intent(in) :: count, nZ, nY
        real*8, intent(in) :: dZ, dY
        integer, intent(in) :: n0
        integer, dimension(nZ, nY), intent(in):: flagMatrix
        real*8, dimension(nZ, nY), intent(in):: potentialMatrix, densityMatrix
        real*8, dimension(nZ * nY), intent(out) :: rightHandSide


        character*4:: count_str

        integer :: IERR

        TYPE(MAT_T) :: MAT
        TYPE(MATVAR_T) :: MATVAR
        integer:: use_compression

        use_compression = 0
        count_str = '0000'
        CALL FMAT_LOGINIT('sm_dump_rhs')


        write(count_str(1:4),'(I4.4)') count / 100

        ! defunct - don't know how to include integers in strings
        write (6,*) 'Writing results to ',trim(mat_file)//'-x_'//count_str//'.mat'

        ierr = FMAT_CREATE(trim(mat_file)//'-x_'//count_str//'.mat'//char(0), MAT)
        ! WRITE FIRST VARIABLE

        ierr = FMAT_VARCREATE('nZ', nZ, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, nZ, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('nY', nY, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, nY, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('dZ', dZ, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, dZ, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('dY', dY, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, dY, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('n0', n0, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, n0, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('flagMatrix', flagMatrix, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, flagMatrix, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('potentialMatrix', potentialMatrix, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, potentialMatrix, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('densityMatrix', densityMatrix, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, densityMatrix, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('rightHandSide', rightHandSide, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, rightHandSide, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_CLOSE(MAT)

    end subroutine sm_dump_result

    subroutine sm_dump_matrix(mat_file, nZ, nY, nZ_disp, flagMatrix, elementCount, dZ, dY, Ai, Aj, Ax)
        USE MATIO

        implicit none

        character*120, intent(in) :: mat_file


        integer, intent(in) :: nZ, nY, nZ_disp
        integer, dimension(nZ, nY), intent(in) :: flagMatrix
        integer, intent(in) :: elementCount
        real*8, intent(in) :: dY, dZ

        integer, dimension(elementCount), intent(in) :: Ai, Aj
        real*8, dimension(elementCount), intent(in) :: Ax


        integer :: IERR

        TYPE(MAT_T) :: MAT
        TYPE(MATVAR_T) :: MATVAR
        integer:: use_compression

        use_compression = 0
        CALL FMAT_LOGINIT('sm_dump_matrix')



        ! defunct - don't know how to include integers in strings
        write (6,*) 'Writing results to ',trim(mat_file)//'-m.mat'

        ierr = FMAT_CREATE(trim(mat_file)//'-m.mat'//char(0), MAT)
        ! WRITE FIRST VARIABLE

        ierr = FMAT_VARCREATE('nZ', nZ, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, nZ, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('nY', nY, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, nY, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('nZ_disp', nZ_disp, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, nZ_disp, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('dZ', dZ, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, dZ, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('dY', dY, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, dY, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('elementCount', elementCount, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, elementCount, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('flagMatrix', flagMatrix, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, flagMatrix, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('Ai', Ai, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, Ai, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('Aj', Aj, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, Aj, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_VARCREATE('Ax', Ax, MATVAR)
        ierr = FMat_VarWrite(mat, matvar, Ax, use_compression)
        ierr = FMAT_VARFREE(MATVAR)

        ierr = FMAT_CLOSE(MAT)

    end subroutine sm_dump_matrix


end module
