module ap_utils

    use mpi

	implicit none

	contains


    subroutine sleep_sync()

        implicit none

        integer :: ierr, mpirank, commsize, request, token, i
        integer, dimension(MPI_STATUS_SIZE) :: mpiStatus
        logical :: completed

        !AP This is a completely random number
        token = 24601

        call MPI_COMM_SIZE(MPI_COMM_WORLD, commsize, ierr)
        call MPI_COMM_RANK(MPI_COMM_WORLD, mpirank, ierr)

        if (mpirank == 0) then
            do i = 1, commsize - 1
                !write (*,*) 'Barrier sending', i, 666
                call MPI_ISEND(token, 1, MPI_INTEGER, i, 666, MPI_COMM_WORLD, request, ierr)
                call MPI_WAIT(request, mpiStatus, ierr)
            enddo
        else
            !write (*,*) 'Barrier waiting', 666
            call MPI_IRECV(token, 1, MPI_INTEGER, 0, 666, MPI_COMM_WORLD, request, ierr)

            completed = .false.

            do while (.not. completed)
                call MPI_TEST(request, completed, mpiStatus, ierr)
                call sleepqq(5);
            enddo
        endif

    end subroutine sleep_sync

	integer function indexOfInt(element, array, arraySize)
		implicit none

		integer, intent(in):: arraySize

		integer, intent(in):: element
		integer, intent(in), dimension(arraySize):: array

		integer :: i, compare

		indexOfInt = -1

		findIndex: do i = 1 , arraySize
			compare = array(i)
			if (compare.eq.element) then
				indexOfInt = i
			endif
		enddo findIndex

		return
	end function indexOfInt

	real*8 function getMass(massInAmu)
		implicit none
		real*8 :: massInAmu
		real*8 :: amu

		amu = 1.660538921e-27

		getMass = massInAmu * amu

		return
	end function getMass

	real*8 function getCharge(chargeInQ0)
		implicit none
		real*8 :: chargeInQ0
		real*8 :: q0

		q0 = 1.60217646e-19

		getCharge = chargeInQ0 * q0

		return
	end function getCharge

	real*8 function getLarmorFrequency(massInAmu, magneticB, chargeInQ0)
		implicit none
		!real*8:: getMass, getCharge

		real*8 :: massInAmu, mass
		real*8 :: chargeInQ0, charge
		real*8 :: magneticB

		mass = getMass(massInAmu)
		charge = getCharge(chargeInQ0)

		getLarmorFrequency = abs(charge) * magneticB / mass

		return
	end function getLarmorFrequency

	real*8 function getKelvins(eV)
		implicit none
		real*8, intent(in) :: eV
		real*8:: kB
		real*8 :: q0

		q0 = 1.60217646e-19
		kB = 1.3806503e-23

		getKelvins = eV * q0 / kB

		return
	end function getKelvins

	real*8 function getDebyeLength(Te, n0)
		implicit none
		!real*8:: getKelvins

		real*8, intent(in) :: Te, n0

		real*8:: q0, kB, epsilon0, TeK

		q0 = 1.60217646e-19
		kB = 1.3806503e-23
		epsilon0 = 8.854187817e-12
		TeK = getKelvins(Te)

		getDebyeLength = sqrt((epsilon0 * kB * TeK) / (q0 * q0 * n0))

		return
	end function getDebyeLength

	integer function checkHalfplane(yA, zA, yB, zB, yC, zC, y1, z1)
		implicit none

		real*8:: yA, zA, yB, zB, yC, zC, yT, yTC, a, b, bc
		real*8:: y1, z1

		logical:: debug
		debug = .false.

		if (debug) then
			write(*,*) 'A: ', zA, yA
			write(*,*) 'B: ', zB, yB
			write(*,*) 'C: ', zC, yC
			write(*,*) 'X: ', z1, y1
		endif

		if (zb.ne.za) then
			!regular line
			a = (yB - yA)/(zB - zA)
			b = - (yB - yA)/(zB - zA)*zA + yA
			bc = -a*zC + yC
			yT = a*z1  + b
			yTC = a*z1 + bc

			if (abs(yT - y1).le.abs(yT - yTC).and.((yT.ge.y1.and.yT.ge.yTC).or.(yT.le.y1.and.yT.le.yTC))) then
				checkHalfplane = 1
			else
				checkHalfplane = 0
			endif
		else
				! straight line/
			if (sign(1.0, real(zC - zA)).eq.sign(1.0, z1 - real(zA)).and.abs(zC - zA) .ge. abs(z1 - zA)) then
				checkHalfplane = 1
			else
				checkHalfplane = 0
			endif
		endif

		return
	end function checkHalfplane

	real*8 function crossProduct2D(vectorA, vectorB)
		implicit none

		real*8, dimension(2):: vectorA, vectorB

		crossProduct2D = 0

		crossProduct2D = vectorA(1) * vectorB(2) - vectorA(2) * vectorB(1)
		return
	end function crossProduct2D


	logical function isInTriangle(y, z, yA, zA, yB, zB, yC, zC)
		implicit none
		!logical:: areOnSameSide

		real*8:: y, z, yA, zA, yB, zB, yC, zC

		real*8, dimension(2):: P, A, B, C

		P = (/ y, z/)
		A = (/ yA, zA/)
		B = (/ yB, zB/)
		C = (/ yC, zC/)

		isInTriangle = .false.

		if (areOnSameSide(P, A, B, C).and.areOnSameSide(P, B, C, A).and.areOnSameSide(P, C, A, B)) then
			isInTriangle = .true.
		endif

		return
	end function isInTriangle

	logical function areOnSameSide(test1, test2, baselineStart, baselineEnd)
		implicit none
		!real*8:: crossProduct2D

		real*8, dimension(2):: test1, test2, baselineStart, baselineEnd

		real*8, dimension(2):: line1, line2, baseline
		real*8:: cross1, cross2

		areOnSameSide = .false.

		baseline = (/ baselineEnd(1) - baselineStart(1), baselineEnd(2) - baselineStart(2) /)
		line1 = (/ test1(1) - baselineStart(1), test1(2) - baselineStart(2) /)
		line2 = (/ test2(1) - baselineStart(1), test2(2) - baselineStart(2) /)

		cross1 = crossProduct2D(baseline, line1)
		cross2 = crossProduct2D(baseline, line2)

		if ((cross1 * cross2).ge.0) then
			areOnSameSide = .true.
		endif

		return
	end function areOnSameSide

	subroutine printMatrix(nRows, nCols, matrix)
		implicit none
		integer, intent(in) :: nRows, nCols
		integer, intent(in), dimension(nRows, nCols) :: matrix
		integer :: row, col, recursionDepth

		integer :: i

		do row = 1, nRows
			do col = 1, nCols
				write (*,'(i$)') matrix(row, col)
			enddo
			write (*,*) ''
		enddo
	end subroutine printMatrix

	subroutine printIntegerMatrix(nRows, nCols, matrix)
		implicit none
		integer, intent(in) :: nRows, nCols
		integer, intent(in), dimension(nRows, nCols) :: matrix
		integer :: row, col, recursionDepth

		integer :: i

		do row = 1, nRows
			do col = 1, nCols
				write (*,'(i$,i$,i$)') row, col, matrix(row, col)
			enddo
			write (*,*) ''
		enddo
	end subroutine printIntegerMatrix

	subroutine printRealMatrix(nRows, nCols, matrix)
		implicit none
		integer, intent(in) :: nRows, nCols
		real*8, intent(in), dimension(nRows, nCols) :: matrix
		integer :: row, col, recursionDepth

		integer :: i

		do row = 1, nRows
			do col = 1, nCols
				write (*,'(f$)') matrix(row, col)
			enddo
			write (*,*) ''
		enddo
	end subroutine printRealMatrix

	subroutine splitInteger8(num, upper, lower)
		integer*8, intent(in) :: num
		integer, intent(out) :: upper, lower

		upper = 0
		lower = 0

		lower = mod(num, 4294967296);
		upper = num / 4294967296;

	end subroutine splitInteger8


	subroutine mergeInteger8(upper, lower, num)
		integer, intent(in) :: upper, lower
		integer*8, intent(out) :: num

		integer*8 :: upperB, lowerB

		upperB = 0
		lowerB = 0
		num = 0
		upperB = upper * 4294967296;
		lowerB = lower * 1
		lowerB = iand(lowerB, 4294967295);
		num = ieor(upperB, lowerB);

	end subroutine mergeInteger8

	subroutine splitInteger8Array(n, array, arrayUpper, arrayLower)

		integer, intent(in) :: n
		integer*8, dimension(n), intent(in) :: array
		integer, dimension(n), intent(out) :: arrayUpper, arrayLower

		integer :: i

		arrayUpper = 0;
		arrayLower = 0;

		do i = 1, n
			call splitInteger8(array(i), arrayUpper(i), arrayLower(i))
		enddo

	end subroutine splitInteger8Array

	subroutine mergeInteger8Array(n, arrayUpper, arrayLower, array)

		integer, intent(in) :: n
		integer, dimension(n), intent(in) :: arrayUpper, arrayLower
		integer*8, dimension(n), intent(out) :: array

		integer :: i

		array = 0;

		do i = 1, n
			call mergeInteger8(arrayUpper(i), arrayLower(i), array(i))
		enddo
	end subroutine mergeInteger8Array

	subroutine splitInteger8Array2D(nx, ny, array, arrayUpper, arrayLower)

		integer, intent(in) :: nx, ny
		integer*8, dimension(nx, ny), intent(in) :: array
		integer, dimension(nx, ny), intent(out) :: arrayUpper, arrayLower

		integer :: i, j

		arrayUpper = 0;
		arrayLower = 0;

		do i = 1, nx
			do j = 1, ny
				call splitInteger8(array(i, j), arrayUpper(i, j), arrayLower(i, j))
			enddo
		enddo

	end subroutine splitInteger8Array2D

	subroutine mergeInteger8Array2D(nx, ny, arrayUpper, arrayLower, array)

		integer, intent(in) :: nx, ny
		integer, dimension(nx, ny), intent(in) :: arrayUpper, arrayLower
		integer*8, dimension(nx, ny), intent(out) :: array

		integer :: i, j

		array = 0;

		do i = 1, nx
			do j = 1, ny
				call mergeInteger8(arrayUpper(i, j), arrayLower(i, j), array(i, j))
			enddo
		enddo
	end subroutine mergeInteger8Array2D

end module ap_utils
