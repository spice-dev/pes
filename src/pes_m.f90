module pes_m
    use mpi

    use ap_utils
    use sm_tools

    implicit none

    include 'dmumps_struc.h'

    contains


    subroutine pes_sync()

        implicit none

        integer :: ierr, mpirank, commsize, request, valjean, i
        integer, dimension(MPI_STATUS_SIZE) :: mpiStatus
        logical :: completed

        !AP This is a completely random number
        valjean = 24601

        call MPI_COMM_SIZE(MPI_COMM_WORLD, commsize, ierr)
        call MPI_COMM_RANK(MPI_COMM_WORLD, mpirank, ierr)

        if (mpirank == 0) then
            do i = 1, commsize - 1
                !write (*,*) 'Barrier sending', i, 666
                call MPI_ISEND(valjean, 1, MPI_INTEGER, i, 666, MPI_COMM_WORLD, request, ierr)
                call MPI_WAIT(request, mpiStatus, ierr)
            enddo
        else
            !write (*,*) 'Barrier waiting', 666
            call MPI_IRECV(valjean, 1, MPI_INTEGER, 0, 666, MPI_COMM_WORLD, request, ierr)

            completed = .false.

            do while (.not. completed)
                call MPI_TEST(request, completed, mpiStatus, ierr)
                call sleepqq(5);
            enddo
        endif

    end subroutine pes_sync

    subroutine pes_solve(mumpsPar, nZ, nY, nZ_disp, dZ, dY, n0, flagMatrix, equipotMatrix, densityMatrix, newPotMatrix, count, tfile)
        implicit none
        ! solver
        type (DMUMPS_STRUC), intent(inout):: mumpsPar

        ! input properties
        integer, intent(in) :: nZ, nY, nZ_disp
        real*8, intent(in) :: dZ, dY
        integer, intent(in) :: n0
        integer, dimension(nZ, nY), intent(in):: flagMatrix
        real*8, dimension(nZ, nY), intent(in):: equipotMatrix, densityMatrix
        real*8, dimension(nZ, nY), intent(out):: newPotMatrix

        integer, intent(in) :: count
        character*120, intent(in) :: tfile

        integer :: z, y, matrixIndex, nYAdjusted, proc_no, ierr

        integer*8 :: timeStart, timeEnd
        real*8 :: timeDiff

        nYAdjusted = nY - 1
        newPotMatrix = 0


        call MPI_COMM_RANK(mumpsPar%COMM, proc_no, ierr)
        !write(*,*) "*** PES ***  mumpsPar%COMM, proc_no, ierr", mumpsPar%COMM, proc_no, ierr

        if (mumpsPar%MYID .eq. 0 .or. proc_no == 0)then

            call sm_rhs(nZ, nYAdjusted, dZ, dY, n0, flagMatrix(1:nZ,1:nYAdjusted), equipotMatrix(1:nZ,1:nYAdjusted), densityMatrix(1:nZ,1:nYAdjusted), mumpsPar%RHS)
            !call printRealMatrix(1, nZ * nYAdjusted, mumpsPar%RHS)
            !write(*,*) ' new rhs -----------------------------------------------------'

            !if (mod(count, 100) .eq. 1) then
            !    call sm_dump_rhs(tfile, count, nZ, nYAdjusted, dZ, dY, n0, flagMatrix(1:nZ,1:nYAdjusted), newPotMatrix(1:nZ,1:nYAdjusted), densityMatrix(1:nZ,1:nYAdjusted), mumpsPar%RHS)
            !endif

        endif

        ! solving call
        mumpsPar%JOB = 3
        call dmumps(mumpsPar)
        ! solver finished

        if (mumpsPar%MYID == 0 .or. proc_no == 0) then

            call sm_result(mumpsPar%NZ, mumpsPar%RHS, nZ, nY, nZ_disp, newPotMatrix)

            !call printRealMatrix(nZ, nY, newPotMatrix)
            !write(*,*) ' new pot -----------------------------------------------------'

            !if (mod(count, 100) .eq. 1) then
            !    call sm_dump_result(tfile, count, nZ, nYAdjusted, dZ, dY, n0, flagMatrix(1:nZ,1:nYAdjusted), newPotMatrix(1:nZ,1:nYAdjusted), densityMatrix(1:nZ,1:nYAdjusted), mumpsPar%RHS)
            !endif

        endif

    end subroutine pes_solve

end module pes_m
