module sorting

	use ap_utils

    implicit none

    contains

    integer function rowcomp_int(nCols, rowA, rowB, sortIndexCount, sortIndices, direction)
        implicit none
        ! A < B -> -1
        ! A = B -> 0
        ! A > B -> 1
        integer, intent(in) :: nCols
        integer, dimension(1, nCols), intent(in) :: rowA, rowB
        integer, intent(in) :: sortIndexCount
        integer, dimension(sortIndexCount), intent(in) :: sortIndices
        integer, intent(in) :: direction

        integer :: colIndex

        rowcomp_int = 0
        !write(*,*) '		$ Comparing'

        do colIndex = 1, sortIndexCount
            if (rowA(1, sortIndices(colIndex)) < rowB(1, sortIndices(colIndex))) then
                rowcomp_int = -1 * direction
                exit
            elseif (rowA(1, sortIndices(colIndex)) > rowB(1, sortIndices(colIndex))) then
                rowcomp_int = 1 * direction
                exit
            endif
        enddo
        !write(*,*) '		', rowA, rowB, compareIntegerRows

        return
    end function rowcomp_int

    subroutine merge_int(nRowsA, nRowsB, nRowsC, nCols, matrixA, matrixB, matrixC, sortIndexCount, sortIndices, direction)
        implicit none

        !integer :: rowcomp_int

        integer, intent(in) :: nRowsA, nRowsB, nRowsC, nCols
        integer, intent(in), dimension(nRowsA, nCols) :: matrixA
        integer, intent(in), dimension(nRowsB, nCols) :: matrixB
        integer, intent(inout), dimension(nRowsC, nCols) :: matrixC

        integer, intent(in) :: sortIndexCount
        integer, dimension(sortIndexCount), intent(in) :: sortIndices
        integer, intent(in) :: direction


        integer, dimension(1, nCols) :: rowA, rowB

        integer :: i, j, k, comparisonResult

        i = 1
        j = 1
        k = 1

        matrixC = 0

        do while (i <= nRowsA .and. j <= nRowsB)
            rowA(1,:) = matrixA(i,:)
            rowB(1,:) = matrixB(j,:)

            comparisonResult = rowcomp_int(nCols, rowA, rowB, sortIndexCount, sortIndices, direction)
            if (comparisonResult <= 0) then
                matrixC(k,:) = matrixA(i,:)
                i = i + 1
            else
                matrixC(k,:) = matrixB(j,:)
                j = j + 1
            endif
            k = k + 1
        enddo

        if (i <= nRowsA) then
            do while (i <= nRowsA)
                matrixC(k,:) = matrixA(i,:)
                i = i + 1
                k = k + 1
            enddo
        endif

        if (j <= nRowsB) then
            do while (j <= nRowsB)
                matrixC(k,:) = matrixB(j,:)
                j = j + 1
                k = k + 1
            enddo
        endif

    ! 	write (*,*) 'Merging C:'
    ! 	call printMatrix(nRowsC, nCols, matrixC)
    end subroutine merge_int


    recursive subroutine mergesort_int(nRows, nCols, matrix, sortIndexCount, sortIndices, direction)

        implicit none
        !integer :: rowcomp_int

        integer, intent(in) :: nRows, nCols
        integer, dimension(nRows, nCols), intent(inout) :: matrix

        integer, intent(in) :: sortIndexCount
        integer, dimension(sortIndexCount), intent(in) :: sortIndices
        integer, intent(in) :: direction

        integer, dimension((nRows + 1)/2, nCols) :: tempMatrix
        integer, dimension(nRows - (nRows + 1)/2, nCols):: tempMatrix1

        integer, dimension(1, nCols) :: row, rowA, rowB
        integer :: nRowsA, nRowsB, comparisonResult
        integer :: i

        rowA = 0
        rowB = 0
        nRowsA = 0
        nRowsB = 0
        comparisonResult = 0


        if (nRows < 2) return
        if (nRows == 2) then
            rowA(1,:) = matrix(1,:)
            rowB(1,:) = matrix(2,:)
            comparisonResult = rowcomp_int(nCols, rowA, rowB, sortIndexCount, sortIndices, direction)
            if (comparisonResult > 0) then
                row(1,:) = matrix(1,:)
                matrix(1,:) = matrix(2,:)
                matrix(2,:) = row(1,:)
            endif
            return
        endif

        nRowsA = (nRows + 1)/2
        nRowsB = nRows - nRowsA

        call mergesort_int(nRowsA, nCols, matrix(1:nRowsA,1:nCols), sortIndexCount, sortIndices, direction)

        call mergesort_int(nRowsB, nCols, matrix(nRowsA + 1:nRows,1:nCols), sortIndexCount, sortIndices, direction)

        rowA(1,:) = matrix(nRowsA,:)
        rowB(1,:) = matrix(nRowsA + 1,:)
        comparisonResult = rowcomp_int(nCols, rowA, rowB, sortIndexCount, sortIndices, direction)
        if (comparisonResult > 0) then
            tempMatrix(1:nRowsA,:) = matrix(1:nRowsA,:)
            tempMatrix1(1:nRowsB,:) = matrix(nRowsA + 1:nRows,:)

            call merge_int(nRowsA, nRowsB, nRows, nCols, tempMatrix, tempMatrix1, matrix, sortIndexCount, sortIndices, direction)
        endif

    end subroutine mergesort_int

end module
