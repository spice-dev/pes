
subroutine loadDimensions(inputFile, nZ, nY, dZ, dY, n0)
	use matio
	implicit none

	character*2048, intent(in):: inputFile

	integer, intent(out) :: nZ, nY
	real*8, intent(out) :: dZ, dY
	integer, intent(out) :: n0

	integer:: use_compression, ierr, version
	type(MAT_T):: MAT
	type(MATVAR_T):: MATVAR

	write (*,*) '% Loading dimensions from file '//trim(inputFile)

	call fmat_loginit('loadArraySize')
	ierr = fmat_open(trim(inputFile)//char(0), mat_acc_rdonly, MAT)
	write (*,*) '#	Open ok', ierr

	ierr = fmat_varreadinfo(MAT, 'Ny'//char(0), MATVAR)
	ierr = fmat_varreaddata(MAT, MATVAR, nY)
	write (*,*) '#	Ny ok', nY

	ierr = fmat_varreadinfo(MAT, 'Nz'//char(0), MATVAR)
	ierr = fmat_varreaddata(MAT, MATVAR, nZ)
	write (*,*) '#	Nz ok', nZ

    dZ = 1.0
	dY = dZ



	!ierr = fmat_varreadinfo(MAT, 'N0'//char(0), MATVAR)
	!ierr = fmat_varreaddata(MAT, MATVAR, n0)

	! npc
	n0 = 50 / (1 * dY * dZ);
	ierr = fmat_close(mat)

end subroutine loadDimensions

subroutine loadFlag(inputFile, nZ, nY, flagMatrix)
	use matio
	implicit none

	character*2048, intent(in):: inputFile

	integer, intent(in):: nZ, nY

	integer, dimension(nZ, nY), intent(out) :: flagMatrix

	integer:: use_compression, ierr, version
	type(MAT_T):: MAT
	type(MATVAR_T):: MATVAR
	integer :: y, z

	write (*,*) '#	Loading flagMatrix from file '//trim(inputFile)

    flagMatrix = 1

	call fmat_loginit('loadArrays')
	ierr = fmat_open(trim(inputFile)//char(0), mat_acc_rdonly, MAT)

	ierr = fmat_varreadinfo(MAT, 'objects'//char(0), MATVAR)
	ierr = fmat_varreaddata(MAT, MATVAR, flagMatrix)

!    do x = 1, nX
!        do y = 1, nY
!            do z = 1, nZ
!                if (flagMatrix(x, y, z) > 0) then
!                    flagMatrix(x, y, z) = 0
!                else
!                    flagMatrix(x, y, z) = 1
!                endif
!            enddo
!        enddo
!    enddo
	ierr = fmat_close(mat)
end subroutine loadFlag

subroutine loadPlasma(inputFile, nZ, nY, potentialMatrix, densityMatrix)
	use matio
	implicit none

	character*2048, intent(in):: inputFile

	integer, intent(in):: nZ, nY

	integer, dimension(nZ, nY), intent(out) :: potentialMatrix, densityMatrix

	integer:: use_compression, ierr, version
	type(MAT_T):: MAT
	type(MATVAR_T):: MATVAR

	write (*,*) '#	Loading input file '//trim(inputFile)

	call fmat_loginit('loadPlasma')
	ierr = fmat_open(trim(inputFile)//char(0), mat_acc_rdonly, MAT)

	ierr = fmat_varreadinfo(MAT, 'Pot'//char(0), MATVAR)
	ierr = fmat_varreaddata(MAT, MATVAR, potentialMatrix)

	ierr = fmat_varreadinfo(MAT, 'rho'//char(0), MATVAR)
	ierr = fmat_varreaddata(MAT, MATVAR, densityMatrix)
	ierr = fmat_close(mat)

end subroutine loadPlasma

subroutine savePlasma(outputFile, nZ, nY, potentialMatrix)
	use matio
	implicit none

	character*2048, intent(in):: outputFile

	integer, intent(in):: nZ, nY

	real*8, dimension(nZ, nY), intent(in) :: potentialMatrix

	integer:: use_compression, ierr, version
	type(MAT_T):: MAT
	type(MATVAR_T):: MATVAR

	use_compression = 0

	write (*,*) '#	Saving output file '//trim(outputFile)

	CALL fmat_loginit('savePlasma')

	ierr = fmat_create(trim(outputFile)//char(0), mat)

	ierr = fmat_varcreate('potentialMatrix', potentialMatrix, matvar)
	ierr = fmat_varwrite(mat, matvar, potentialMatrix, use_compression)
	ierr = fmat_varfree(matvar)

	ierr = fmat_close(mat)
	write (6,*) '#	Plasma written.'

end subroutine savePlasma

subroutine saveProgress(outputFile, nZ, nY, potentialMatrix, potentialMatrix2, densityMatrix, flagMatrix, steps, stats, init_time, n_procs, mem_usage, vm_size)
	use matio
	implicit none

	character*2048, intent(in):: outputFile

	integer, intent(in):: nZ, nY

	real*8, dimension(nZ, nY), intent(in) :: potentialMatrix, potentialMatrix2, densityMatrix
	integer, dimension(nZ, nY), intent(in) :: flagMatrix
	integer, intent(in) :: steps
	real*8, dimension(steps) :: stats
	real*8, intent(in) :: init_time


    integer, intent(in) :: n_procs
    integer*8, dimension(steps + 1, n_procs), intent(in) :: mem_usage, vm_size
    real*8, dimension(steps + 1, n_procs) :: mem_usage_r, vm_size_r


	integer:: use_compression, ierr, version
	type(MAT_T):: MAT
	type(MATVAR_T):: MATVAR

	use_compression = 0
	mem_usage_r = dble(mem_usage)
	vm_size_r = dble(vm_size)

	write (*,*) '#	Saving output file '//trim(outputFile)

	CALL fmat_loginit('savePlasma')

	ierr = fmat_create(trim(outputFile)//char(0), mat)

	ierr = fmat_varcreate('potentialInput', potentialMatrix, matvar)
	ierr = fmat_varwrite(mat, matvar, potentialMatrix, use_compression)
	ierr = fmat_varfree(matvar)

	ierr = fmat_varcreate('potentialMatrix', potentialMatrix2, matvar)
	ierr = fmat_varwrite(mat, matvar, potentialMatrix2, use_compression)
	ierr = fmat_varfree(matvar)

	ierr = fmat_varcreate('flagMatrix', flagMatrix, matvar)
	ierr = fmat_varwrite(mat, matvar, flagMatrix, use_compression)
	ierr = fmat_varfree(matvar)

	ierr = fmat_varcreate('densityMatrix', densityMatrix, matvar)
	ierr = fmat_varwrite(mat, matvar, densityMatrix, use_compression)
	ierr = fmat_varfree(matvar)

	ierr = fmat_varcreate('steps', steps, matvar)
	ierr = fmat_varwrite(mat, matvar, steps, use_compression)
	ierr = fmat_varfree(matvar)

	ierr = fmat_varcreate('stats', stats, matvar)
	ierr = fmat_varwrite(mat, matvar, stats, use_compression)
	ierr = fmat_varfree(matvar)

	ierr = fmat_varcreate('init_time', init_time, matvar)
	ierr = fmat_varwrite(mat, matvar, init_time, use_compression)
	ierr = fmat_varfree(matvar)

	ierr = fmat_varcreate('mem_usage', mem_usage_r, matvar)
	ierr = fmat_varwrite(mat, matvar, mem_usage_r, use_compression)
	ierr = fmat_varfree(matvar)

	ierr = fmat_varcreate('vm_size', vm_size_r, matvar)
	ierr = fmat_varwrite(mat, matvar, vm_size_r, use_compression)
	ierr = fmat_varfree(matvar)

	ierr = fmat_close(mat)
	write (6,*) '#	Progress written.'

end subroutine saveProgress

subroutine saveMatrix(outputFile, elementCount, matrixDimension, Ai, Aj, Ax)
	use matio
	implicit none

	character*2048, intent(in):: outputFile

	integer, intent(in) :: elementCount, matrixDimension
	integer, dimension(elementCount), intent(in) :: Ai, Aj
	real*8, dimension(elementCount), intent(in) :: Ax

	integer:: use_compression, ierr, version
	type(MAT_T):: MAT
	type(MATVAR_T):: MATVAR

	use_compression = 0

	write (*,*) '#	Saving output file '//trim(outputFile)
	write (*,*) '#	init'

	CALL fmat_loginit('saveMatrix')
	write (*,*) '#	init ok', elementCount, matrixDimension

	ierr = fmat_create(trim(outputFile)//char(0), mat)

	ierr = fmat_varcreate('elementCount', elementCount, matvar)
	ierr = fmat_varwrite(mat, matvar, elementCount, use_compression)
	ierr = fmat_varfree(matvar)
	write (*,*) '#	elementCount', ierr

	ierr = fmat_varcreate('matrixDimension', matrixDimension, matvar)
	ierr = fmat_varwrite(mat, matvar, matrixDimension, use_compression)
	ierr = fmat_varfree(matvar)
	write (*,*) '#	matrixDimension', ierr

	ierr = fmat_varcreate('Ai', Ai, matvar)
	ierr = fmat_varwrite(mat, matvar, Ai, use_compression)
	ierr = fmat_varfree(matvar)
	write (*,*) '#	Ai', ierr

	ierr = fmat_varcreate('Aj', Aj, matvar)
	ierr = fmat_varwrite(mat, matvar, Aj, use_compression)
	ierr = fmat_varfree(matvar)
	write (*,*) '#	Aj', ierr

	ierr = fmat_varcreate('Ax', Ax, matvar)
	ierr = fmat_varwrite(mat, matvar, Ax, use_compression)
	ierr = fmat_varfree(matvar)
	write (*,*) '#	Ax', ierr

	ierr = fmat_close(mat)
	write (6,*) '#	Matrix written.'

end subroutine saveMatrix


subroutine saveMatrixCsr(outputFile, elementCount, matrixDimension, Bi, Bp, Bx)
	use matio
	implicit none

	character*2048, intent(in):: outputFile

	integer, intent(in) :: elementCount, matrixDimension
	integer, dimension(elementCount), intent(in) :: Bi
	integer, dimension(matrixDimension + 1), intent(in) :: Bp
	real*8, dimension(elementCount), intent(in) :: Bx

	integer:: use_compression, ierr, version
	type(MAT_T):: MAT
	type(MATVAR_T):: MATVAR

	use_compression = 0

	write (*,*) '#	Saving output file '//trim(outputFile)

	CALL fmat_loginit('saveMatrix')

	ierr = fmat_create(trim(outputFile)//char(0), mat)

	ierr = fmat_varcreate('elementCount', elementCount, matvar)
	ierr = fmat_varwrite(mat, matvar, elementCount, use_compression)
	ierr = fmat_varfree(matvar)


	ierr = fmat_varcreate('matrixDimension', matrixDimension, matvar)
	ierr = fmat_varwrite(mat, matvar, matrixDimension, use_compression)
	ierr = fmat_varfree(matvar)


	ierr = fmat_varcreate('Bi', Bi, matvar)
	ierr = fmat_varwrite(mat, matvar, Bi, use_compression)
	ierr = fmat_varfree(matvar)

	ierr = fmat_varcreate('Bp', Bp, matvar)
	ierr = fmat_varwrite(mat, matvar, Bp, use_compression)
	ierr = fmat_varfree(matvar)

	ierr = fmat_varcreate('Bx', Bx, matvar)
	ierr = fmat_varwrite(mat, matvar, Bx, use_compression)
	ierr = fmat_varfree(matvar)

	ierr = fmat_close(mat)
	write (6,*) '#	CSR matrix written.'

end subroutine saveMatrixCsr
