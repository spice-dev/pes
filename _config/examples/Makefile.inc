# This is a include file for make.
#
# You can either use the file without any changes, or you should specify your own paths.
#
# 	*	If no changes are made, you should compile all external libraries first.
# 	*	If you are using your own libraries, be sure that all of them are compiled with the same compiler (eg. do not mix intelmpi and openmpi etc.). Change only appropriate assignments then.

# standard makefile shell assignment
SHELL = /bin/sh

FC=ifort
MPIF90=mpiifort

DEBUG_FLAGS=-C -g -check all -check noarg_temp_created
RELEASE_FLAGS=-DALLOW_NON_INIT -O3 -axCORE-AVX2 -xSSE4.2 -qopt-report1 -qopt-report-phase=vec -qoverride-limits -qopenmp -fPIC
#DEBUG_FLAGS=
#FC_FLAGS=-fPIC -r8 -axCORE-AVX2 -xSSE4.2
FC_FLAGS=-DPES_TESTING -fPIC -qopenmp

PICLIB_DIR=/home/podolnik/Coding/spice/spice-lib/_source

MEMUSAGE_LIB=$(PICLIB_DIR)/memusage/memusage/lib/libmemusage.a
CLOCK_LIB=$(PICLIB_DIR)/clock/clock/lib/libclock.a
MATIO_LIBS=-I$(PICLIB_DIR)/matio/matio-1.3.4/src $(PICLIB_DIR)/matio/matio-1.3.4/src/.libs/libmatio.a -lz

MPI_LIBS=-lmpi
COMMON_LIBS=-L$(USR_LIB_DIR) $(MPI_LIBS) -lutil -ldl -lpthread


SCOTCH_LIBDIR=$(PICLIB_DIR)/scotch/scotch_5.1.12_esmumps/lib
MUMPS_LIBDIR=$(PICLIB_DIR)/mumps/MUMPS_4.10.0/lib
MUMPS_INCDIR=$(PICLIB_DIR)/mumps/MUMPS_4.10.0/include

SCOTCH_LIBDIR=$(PICLIB_DIR)/scotch/scotch_6.0.6/lib
MUMPS_LIBDIR=$(PICLIB_DIR)/mumps/MUMPS_5.1.2/lib
MUMPS_INCDIR=$(PICLIB_DIR)/mumps/MUMPS_5.1.2/include

SCOTCH_LIBS=-L$(SCOTCH_LIBDIR) -lptesmumps -lscotch -lptscotch -lptscotcherr
MUMPS_LIBS_BASIC=$(MUMPS_LIBDIR)/libdmumps.a $(MUMPS_LIBDIR)/libmumps_common.a
MUMPS_LIBS=$(MUMPS_LIBS_BASIC) $(SCOTCH_LIBS)

MKL_LIBS=$(MKLROOT)/lib/intel64/libmkl_blas95_lp64.a $(MKLROOT)/lib/intel64/libmkl_lapack95_lp64.a $(MKLROOT)/lib/intel64/libmkl_scalapack_lp64.a -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_cdft_core.a $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_intel_thread.a $(MKLROOT)/lib/intel64/libmkl_core.a $(MKLROOT)/lib/intel64/libmkl_blacs_intelmpi_lp64.a -Wl,--end-group -liomp5 -lpthread -lm -ldl
MKL_INCLUDE=-I$(MKLROOT)/include/intel64/lp64 -I$(MKLROOT)/include

ALL_LIBS=$(CLOCK_LIB) $(MATIO_LIBS) $(MUMPS_LIBS)

SUPPORT_LIBS=$(MKL_LIBS)

ALL_LIBS=$(MATIO_LIBS) $(MUMPS_LIBS) $(CLOCK_LIB) $(MEMUSAGE_LIB) $(SUPPORT_LIBS) $(COMMON_LIBS)
MPIF_INCLUDE=$(MKL_INCLUDE) -I$(MUMPS_INCDIR)
