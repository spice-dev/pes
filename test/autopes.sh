
export OMP_NUM_THREADS=1

rm *.log

MPI_PROC=4
TEST_NAME=auto128-256-1
LOG_DIR=logs/$TEST_NAME/$MPI_PROC
mkdir -p $LOG_DIR
mpirun -np $MPI_PROC -outfile-pattern=$LOG_DIR/test-$TEST_NAME.%r.log ../bin/autopes -o $LOG_DIR/$TEST_NAME-o.mat -y 128 -z 256

echo "auto pes test done"
