# PES
Aka Poisson equation solver.
Intended as a replacement for old solver in SPICE2.
## Quick build
- Create a `Makefile.inc` according to example provided.
- Run `make default` to create solver with centralized matrix storage (see more options in `makefile`).